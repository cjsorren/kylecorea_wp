<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package kylecorea
 */

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <div class="full-image">
            <?php

                                  $image = CFS()->get( 'image' );
                                  $size = 'full'; // (thumbnail, medium, large, full or custom size)

                                  if( $image ) {

                                    echo wp_get_attachment_image( $image, $size );

                                  }

            ?>
            </div>


<?php endwhile; else: ?>

<p>There are no posts or pages here</p>

<?php endif; ?>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php get_footer(); ?>
