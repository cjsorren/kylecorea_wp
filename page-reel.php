<?php
/**
 * The template for displaying About Page.
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package kylecorea
 */

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

        <section class="reel">
        <div>
            <iframe src="https://player.vimeo.com/video/<?php echo CFS()->get( 'vimeo_id' ); ?>" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

        </div>

        </section>



        </main><!-- #main -->
    </div><!-- #primary -->


<?php get_footer(); ?>
