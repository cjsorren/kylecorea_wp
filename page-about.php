<?php
/**
 * The template for displaying About Page.
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package kylecorea
 */

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

        <section class="about">
        <div>
            <h2><?php echo CFS()->get( 'about_heading' ); ?></h2>
            <?php echo CFS()->get( 'about_text' ); ?>
        </div>


        <div class="about-list">
            <h2><?php echo CFS()->get( 'list_heading' ); ?></h2>
            <ul>
            <?php
                $fields = CFS()->get( 'list' );
                foreach ( $fields as $field ) {
                    echo '<li>' . $field['list_item'] . '</li>';
                }
            ?>
            </ul>

        </div>
        </section>



        </main><!-- #main -->
    </div><!-- #primary -->


<?php get_footer(); ?>
