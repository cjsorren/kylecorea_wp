<div class="grid">
  <div class="grid-sizer"></div>
  <div class="gutter-sizer"></div>
<?php

    $args = array (
        'post_type' => 'portfolio'
    );

    $the_query = new WP_Query( $args );

?>

<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

        <!--<img class="project-image" src="<?php echo CFS()->get( 'image' ); ?>" />-->


        <div class="grid-item">
            <a href="<?php echo get_permalink(); ?>">
                <?php

                              $image = CFS()->get( 'image' );
                              $size = 'full'; // (thumbnail, medium, large, full or custom size)

                              if( $image ) {

                                echo wp_get_attachment_image( $image, $size );

                              }

                ?>
            </a>
        </div>

<?php endwhile; else: ?>

<p>There are no posts or pages here</p>

<?php endif; ?>

</div><!-- #Grid -->