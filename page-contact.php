<?php
/**
 * The template for displaying Contact Page.
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package kylecorea
 */

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

        <section class="contact">
        <div>
            <a href="mailto:<?php echo CFS()->get( 'email' ); ?>"><?php echo CFS()->get( 'email' ); ?></a>

        </div>
        </section>


        </main><!-- #main -->
    </div><!-- #primary -->


<?php get_footer(); ?>
