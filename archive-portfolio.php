<?php
/**
 * The template for displaying Work Page.
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package kylecorea
 */

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

        <?php get_template_part( 'grid' ); ?>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php get_footer(); ?>
