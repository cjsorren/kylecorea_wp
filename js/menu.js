jQuery(document).ready(function( $ ) {

var sidebarTrigger = $('.cd-nav-trigger'),
    sidebar = $('.main-navigation');

//mobile only - open sidebar when user clicks the hamburger menu
sidebarTrigger.on('click', function(event){
  event.preventDefault();
  $([sidebar, sidebarTrigger]).toggleClass('nav-is-visible');
});

//click on item and show submenu
$('.menu-item-has-children > a').on('click', function(event){
  var mq = checkMQ(),
    selectedItem = $(this);
  if( mq === 'mobile' || mq === 'tablet' ) {
    event.preventDefault();
    if( selectedItem.parent('li').hasClass('selected')) {
        selectedItem.parent('li').removeClass('selected');
    } else {
      sidebar.find('.menu-item-has-children.selected').removeClass('selected');
      selectedItem.parent('li').addClass('selected');
    }
  }


});

function checkMQ() {
  //check if mobile or desktop device
  return window.getComputedStyle(document.querySelector('.site-content'), '::before').getPropertyValue('content').replace(/'/g, "").replace(/"/g, "");
}

//$('#site-navigation').on('click','.menu-toggle', function() {
//  $('.menu').slideToggle('fast', function() {
//    if($('.menu').is(':visible')) {
//      $('.menu-toggle span').html('&#9650;');
//    } else {
//      $('.menu-toggle span').html('&#9660;');
//    }
//  });
//});
//
//$('.main-navigation').on('click', '.menu-item-has-children a', function() {
//  if ($(window).width() < 772 || $('html').hasClass('touch')) {
//    if ($(this).next('ul').is(':visible')) {
//      $(this).next('ul').slideUp('fast');
//      $(this).removeClass('active');
//    } else {
//      $(this).closest('ul').find('.active').next('ul').slideUp('fast');
//      $(this).closest('ul').find('.active').removeClass('active');
//      $(this).next().slideToggle('fast');
//      $(this).addClass('active');
//    }
//  }
//});

});